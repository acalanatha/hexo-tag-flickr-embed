A Hexo plugin for embedding Flickr photos in your site. [Demo](https://blog.godspeedyou.tokyo/2018/05/27/hexo-tag-flickr-embed-demo/)

## Installation

```
npm install hexo-tag-flickr-embed --save
```

## Configuration

```
flickr_embed:
  api_key: foobar
  cache_file_path: hexo-tag-flickr-embed-cache.json
  cache_expires: 8640000000
```

## Usage

Please pass Flickr photoID for the first arg.

```
{% flickr_embed 41099647555 %}
```

To change the image size, Use the 2nd arg.

```
{% flickr_embed 41099647555 "Large Square" %}
```

To enable the header and footer, Use 3rd & 4th args.

```
{% flickr_embed phpto_id "Large Square" true true %}
```

## Arguments 

| Order | Name         | Required | Default      | Description                                                                                                                             | Example     |
|-------|--------------|----------|--------------|-----------------------------------------------------------------------------------------------------------------------------------------|-------------|
| 1     | photo ID     | Yes      |              | The identifier of the photo to fetch description for.                                                                                   | 123456789   |
| 2     | Image size   | No       | "Medium 640" | See [Flickr API reference](https://www.flickr.com/services/api/flickr.photos.getSizes.html). | "Small 320" |
| 3     | Header       | No       | "false"      |                                                                                                                                         | "true"      |
| 4     | Footer       | No       | "false"      |                                                                                                                                         | "true"      |
| 5     | Photo stream | No       | "false"      |                                                                                                                                         | "true"      |
|       |              |          |              |                                                                                                                                         |             |

## Advanced Configuration
You can overwrite default variables by the cfg.

```
flickr_embed:
  size: "Large"
  header: true
  footer: true
  context: false
```
