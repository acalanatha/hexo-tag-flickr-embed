import Program from './class/program'

hexo.extend.tag.register(
  'flickr_embed',
  function(args) {
    return Program.main(args, hexo)
      .then(tag => {
        return tag
      })
      .catch(err => {
        hexo.log.error(err)
        throw err
      })
  },
  {
    async: true,
    end: false
  }
)
