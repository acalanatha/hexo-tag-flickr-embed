import Flickr from 'flickr-sdk'
import InCache from 'incache'
import md5 from 'md5'

/**
 * flickr-sdkをcacheするwrapper, sdkにpaththroughする
 */
export default class FlickrClient {
  constructor(api_key, cacheFilePath, cacheTTL) {
    this.sdk = new Flickr(api_key)
    this.cacheTTL = cacheTTL
    this.cacheStorage = cacheFilePath
      ? new InCache({ autoSave: true, filePath: cacheFilePath })
      : new InCache()

    // @see https://stackoverflow.com/questions/37714787/can-i-extend-proxy-with-an-es2015-class
    return new Proxy(this.sdk, {
      get: (target, prop) => {
        if (target instanceof Flickr) {
          let namespace = prop
          return new Proxy(
            {},
            {
              get: (target, method) => {
                return function() {
                  return this.__callSdkMethod(namespace, method, arguments)
                }.bind(this)
              }
            }
          )
        }

        return target[prop]
      }
    })
  }

  __callSdkMethod(namespace, method, args) {
    let token = md5(JSON.stringify(arguments))
    let cachedResp = this.cacheStorage.get(token)

    if (cachedResp) {
      console.log('has cache')
      return new Promise(() => {
        return cachedResp
      })
    }

    return this.sdk[namespace][method](args[0])
      .then(
        function(resp) {
          this.cacheStorage.set(
            token,
            resp,
            this.cacheTTL ? { maxAge: this.cacheTTL } : {}
          )
          return resp
        }.bind(this)
      )
      .catch(function(err) {
        throw err
      })
  }
}
