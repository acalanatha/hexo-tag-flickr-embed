import TagBuilder from './tagbuilder'
import FlickrClient from './flickrclient'
import Enumerable from 'linq'

const DEFAULT_IMG_SIZE = 'Medium 640'
const ERR_MSG_PREFIX = '# hexo-tag-flickr-embed #'

export default class Program {
  static /* Promise */ main(args, hexo) {
    let config = hexo.config.flickr_embed
    if (!config.api_key) throw `${ERR_MSG_PREFIX} api_key is required.`

    let photoId = args[0]

    // 少し汚い
    // prettier-ignore
    let size    = args[1] !== undefined ? args[1] : (config.size  !== undefined ? config.size : DEFAULT_IMG_SIZE)
    // prettier-ignore
    let header  = args[2] !== undefined ? (args[2].toLowerCase() == "true") : (config.header  !== undefined ? config.header  : false)
    // prettier-ignore
    let footer  = args[3] !== undefined ? (args[3].toLowerCase() == "true") : (config.footer  !== undefined ? config.footer  : false)
    // prettier-ignore
    let context = args[4] !== undefined ? (args[4].toLowerCase() == "true") : (config.context !== undefined ? config.context : false)

    let flickr = new FlickrClient(
      config.api_key,
      config.cache_file_path | false,
      config.cache_expires | false
    )

    return Promise.all([
      flickr.photos.getInfo({ photo_id: photoId }),
      flickr.photos.getSizes({ photo_id: photoId })
    ])
      .then(res => {
        let sizeCtx = Enumerable.from(res[1].body.sizes.size)
          .where(x => x.label == size)
          .elementAtOrDefault(0, null)
        if (!sizeCtx)
          throw `${ERR_MSG_PREFIX} No expected image size "${size}".`

        let urlCtx = Enumerable.from(res[0].body.photo.urls.url)
          .where(x => x.type == 'photopage')
          .elementAtOrDefault(0, null)
        let titleCtx = res[0].body.photo.title

        // @todo factoryにまとめる
        let tb = new TagBuilder()
        tb.photoPageUrl = urlCtx._content
        tb.title = titleCtx._content
        tb.imageUrl = sizeCtx.source
        tb.width = sizeCtx.width
        tb.height = sizeCtx.height
        tb.header = header
        tb.context = context
        tb.footer = footer

        return tb.toString()
      })
      .catch(err => {
        throw err
      })
  }
}
