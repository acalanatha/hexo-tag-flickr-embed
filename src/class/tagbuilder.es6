import escapeHtml from 'escape-html'

export default class TagBuilder {
  constructor() {
    this.photoPageUrl
    this.title
    this.imageUrl
    this.header = false
    this.footer = false
    this.context = false
    this.width
    this.height
  }

  toString() {
    // @todo DOM-manipulation or .ejs
    return `
<a data-flickr-embed="true"
  href="${escapeHtml(this.photoPageUrl)}"
  title="${escapeHtml(this.title)}"
  data-header="${this.header ? 'true' : 'false'}"
  data-footer="${this.footer ? 'true' : 'false'}"
  data-context="${this.context ? 'true' : 'false'}"
>
<img
  src="${escapeHtml(this.imageUrl)}"
  width="${escapeHtml(this.width)}"
  height="${escapeHtml(this.height)}"
  alt="${escapeHtml(this.title)}">
</a>
<script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
`.replace(/\r?\n/g, '')
  }
}
